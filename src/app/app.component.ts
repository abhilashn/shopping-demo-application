import { Component } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';
import {
  Event as RouterEvent,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError,
  Router,
  ActivatedRoute
} from '@angular/router';
import { HttpService } from './_services/http.service';
import { AuthenticationService } from './_services/authentication.service';
import { MessageService } from './_services/message-servics';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({ transform: 'translateX(100%)', opacity: 0 }),
          animate('500ms', style({ transform: 'translateX(0)', opacity: 1 }))
        ]),
        transition(':leave', [
          style({ transform: 'translateX(0)', opacity: 1 }),
          animate('500ms', style({ transform: 'translateX(100%)', opacity: 0 }))
        ])
      ]
    )
  ]
})
export class AppComponent {
  title = 'app';
  message: any;
  constructor(private router: Router,
    private httpService: HttpService,
    private authService: AuthenticationService,
    private msgService: MessageService) {
    router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event);
    });

    msgService.msgUpdated$.subscribe(
      data => {
        this.message = [];
        this.message = data;
        this.triggerTimmer();
        console.log(this.message);
      });
  }
  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      if (this.authService.isLoggedIn) {
        const currentUrl = event.url;
        if (currentUrl == '/app' || currentUrl == '/') {
          this.router.navigate(['app', 'products']);  // should loaded according to menu sevice
        }
      }
    }
  }
  triggerTimmer() {
    setInterval(() => {
      this.message = [];
    }, 2000)
  }
}
