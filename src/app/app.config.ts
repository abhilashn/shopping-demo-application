import { Inject, Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AppConfig {

    public config: any = null;
    private env: Object = null;

    constructor(private http: Http) {
        console.log('constructor AppConfig');
    }

    /**
     * Use to get the data found in the second file (config file)
     */
    public getConfig(key: any) {
        return this.config[key];
    }

    /**
     * Use to get the data found in the first file (env file)
     */
    public getEnv(key: any) {
        return this.env[key];
    }

    /**
     * This method:
     *   a) Loads "env.json" to get the current working environment (e.g.: 'production', 'development')
     *   b) Loads "config.[env].json" to get all env's variables (e.g.: 'config.development.json')
     */
    public load() {
        return new Promise((resolve, reject) => {
            this.http.get('assets/config.json').map(res => res.json()).catch((error: any): any => {
                console.log('app error', error);
                resolve(true);
                return Observable.throw(error.json().error || 'Server error');
            }).subscribe((envResponse) => {
                // this.env = envResponse;
                this.config = envResponse;
                // document.title = this.config.siteTitle;
                resolve(true);
            });

        });
    }
}
