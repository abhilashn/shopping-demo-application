import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { HttpService } from '../../_services/http.service';
import { AuthenticationService } from '../../_services/authentication.service';
import { Router } from '@angular/router';
import { MessageService } from '../../_services/message-servics';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signUpForm: FormGroup;
  submittedForm = false;
  errormMsg = {};
  hide = true;
  retypehide = true;

  constructor(private httpService: HttpService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private msgService: MessageService
  ) { }

  ngOnInit() {
    this.signUpForm = new FormGroup({
      'name': new FormControl(null, Validators.required),
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, Validators.required),
      're-password': new FormControl(null, Validators.required)
    });
    this.resetErroMsg();
  }

  formSubmitted(form) {
    this.submittedForm = true;
    if (form.valid) {
      this.httpService.post('signup', form.value).subscribe(res => {
        // console.log(res);
        if (res.success) {
          let result: any = {};
          // result = res;
          // Toaster msg
          this.msgService.updateMsg({'message': res.message, 'success': res.success});
          this.router.navigate(['/sign-in']);
        } else {
          this.displayErrorMsg(res);
        }
      }, error => {
        // console.log('Error');
        this.msgService.updateMsg({'message': 'Something went wrong', 'success': false});
      });
    }
  }

  resetErroMsg(){
    this.errormMsg = {
      'name': '',
      'email': '',
      'password': '',
      're-password': ''
    };
  }

  displayErrorMsg(result){
    this.resetErroMsg();
    if (!result.success){
      this.msgService.updateMsg({'message': result.message, 'success': result.success});
      if (result.error.constructor === Array){
        this.errormMsg = result.error[0];
      }
    }
  }

}
