import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatMenuModule, MatIconModule, MatToolbarModule, MatFormFieldModule, MatInputModule, MatCardModule } from '@angular/material';

import { PagesRoutingModule } from './pages-routing.module';

import { SignupComponent } from './signup/signup.component';
import { SigninComponent } from './signin/signin.component';

const materialModules = [
  MatButtonModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatFormFieldModule,
  MatInputModule,
  MatCardModule
];

@NgModule({
  declarations: [
    SigninComponent,
    SignupComponent
  ],
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    PagesRoutingModule,
    materialModules
  ],
  exports: [materialModules],
  providers: [],
  bootstrap: []
})
export class PagesModule { }
