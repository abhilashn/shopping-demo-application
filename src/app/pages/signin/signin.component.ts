import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

import { HttpService } from '../../_services/http.service';
import { AuthenticationService } from '../../_services/authentication.service';
import { MessageService } from '../../_services/message-servics';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  signInForm: FormGroup;
  user = {};
  errormMsg = {};
  submittedForm = false;
  hide = true;

  constructor(
    private httpService: HttpService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private msgService: MessageService
  ) { 
    // console.log('Const Login');
  }

  ngOnInit() {
    this.signInForm = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, Validators.required),
    });
    this.resetErroMsg();
  }

  formSubmitted(form) {
    this.submittedForm = true;
    if (form.valid) {
      this.httpService.post('signin', form.value).subscribe(res => {
        if (res.success) {
          let result: any = {};
          result = res;
          this.msgService.updateMsg({'message': result.message, 'success': result.success});
          this.authenticationService.login(result.data,result.token);
          this.router.navigate(['/app']);
        } else {
          this.displayErrorMsg(res);
        }
      }, error => {
        console.log('Error');
        this.msgService.updateMsg({'message': 'Something went wrong', 'success': false});
      });
    }
  }

  resetErroMsg(){
    this.errormMsg = {
      'email': '',
      'password': ''
    };
  }

  displayErrorMsg(result){
    this.resetErroMsg();
    if (!result.success){
      this.msgService.updateMsg({'message': result.message, 'success': result.success});
      if (result.error.constructor === Array){
        this.errormMsg = result.error[0];
      }
    }
  }

}
