import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpService } from '../../_services/http.service';
import { LocalStorageService } from '../../_services/local-storage.service';
import { CartService } from '../../_services/cart-servics';
import { MessageService } from '../../_services/message-servics';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  cartData = [];
  userId = '';
  @Output() reloaddata = new EventEmitter();

  constructor(private httpService: HttpService, 
    private localStorage: LocalStorageService, 
    private cartService: CartService,
    private msgService: MessageService
  ) { }

  ngOnInit() {
    this.userId = JSON.parse(this.localStorage.getItem('currentUser')).uid;
    // cart data
    this.getCartList();
  }

  getCartList(){
    this.httpService.get('api/cart/get/' + this.userId, {}).subscribe(res => {
      this.cartData = res.data;
      this.cartService.updateCart(res.data);
    }, error => {
      console.log('Error');
    });
  }

  deleteProduct(product){
    // console.log(product);
    this.httpService.get('api/cart/delete/' + product.ucid +'/'+ product.pid, {}).subscribe(res => {
      this.msgService.updateMsg({'message': res.message, 'success': res.success});
      this.getCartList();
    }, error => {
      // console.log('Error');
      this.msgService.updateMsg({'message': 'Something went wrong', 'success': false});
    });
  }

  changeCount(product, event) {
    console.log(product, event);
    product.uid = this.userId;
    this.httpService.post('api/cart/update/', product).subscribe(res => {
      if (res.success) {
        this.getCartList();
        this.msgService.updateMsg({'message': res.message, 'success': res.success});
      } else {
        // console.log('Error', res.message);
        this.msgService.updateMsg({'message': res.message, 'success': res.success});
      }
    }, error => {
      console.log('Error');
      this.msgService.updateMsg({'message': 'Something went wrong', 'success': false});
    });
  }

}
