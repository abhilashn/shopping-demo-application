import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../_services/http.service';
import { LocalStorageService } from '../../_services/local-storage.service';
import { CartService } from '../../_services/cart-servics';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { MessageService } from '../../_services/message-servics';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  constructor(private httpService: HttpService, 
    private localStorage: LocalStorageService, 
    private cartService: CartService,
    private http: HttpClient,
    private msgService: MessageService
  ) { }

  productList = [];
  userId = '';
  ngOnInit() {
    this.userId = JSON.parse(this.localStorage.getItem('currentUser')).uid;
    this.getProductList();
  }

  getProductList(){
    this.productList = [];
    this.httpService.get('api/product/list', {}).subscribe(res => {
      if (res.success) {
        this.productList = res.data;
      }
    }, error => {
      console.log('Error');
    });
  }

  addToCart(product){
    product.uid = JSON.parse(this.localStorage.getItem('currentUser')).uid;
    this.httpService.post('api/add-cart', product).subscribe(res => {
      if (res.success) {
        this.getCartCount();
        this.getProductList();
        this.msgService.updateMsg({'message': res.message, 'success': res.success});
      } else {
        this.msgService.updateMsg({'message': res.message, 'success': res.success});
      }
    }, error => {
      console.log('Error');
    });
  }

  getCartCount(){
    this.httpService.get('api/cart/get/' + this.userId, {}).subscribe(res => {
      this.cartService.updateCart(res.data);
    }, error => {
      console.log('Error');
    });
  }

}
