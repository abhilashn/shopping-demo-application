import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatMenuModule, MatIconModule, MatToolbarModule, MatFormFieldModule, MatInputModule, MatCardModule } from '@angular/material';
import { AuthRoutingModule } from './auth-routing.module';
import { ProductListComponent } from '../../mod/product-list/product-list.component';

const materialModules = [
  MatButtonModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatFormFieldModule,
  MatInputModule,
  MatCardModule
];

@NgModule({
  declarations: [
    ProductListComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    materialModules,
    ReactiveFormsModule,
    AuthRoutingModule
  ],
  exports: [materialModules],
  providers: [],
  bootstrap: []
})
export class AuthModule { }
