import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductListComponent } from '../../mod/product-list/product-list.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'app/products',
    pathMatch: 'full'
  },
  {
    path: 'products',
    component: ProductListComponent,
    data: {
      title: 'Products'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
