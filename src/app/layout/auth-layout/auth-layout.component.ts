import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../_services/http.service';
import { LocalStorageService } from '../../_services/local-storage.service';
// import { CartComponent } from '../../mod/cart/cart.component';
import { CartService } from '../../_services/cart-servics';
import { AuthenticationService } from '../../_services/authentication.service';

@Component({
  selector: 'app-auth-layout',
  templateUrl: './auth-layout.component.html',
  styleUrls: ['./auth-layout.component.css']
})
export class AuthLayoutComponent implements OnInit {

  menuData: any;
  cartData = [];
  userId = '';
  showCart=false;

  constructor(private httpService: HttpService, private localStorage: LocalStorageService, private cartService: CartService, private authService : AuthenticationService) { 
    cartService.cartUpdated$.subscribe(
      data => {
        this.cartData = [];
        data.forEach(element => {
          if (element != ''){
            this.cartData.push(element);
          }
        });
        console.log(this.cartData);
      });
      /* cartService.cartUpdated$.subscribe(
        astronaut => {
          this.cartData = [];
          astronaut.forEach(element => {
            if (element != ''){
              this.cartData.push(element);
            }
          });
          // this.cartData.push(`${astronaut}`);
          console.log(this.cartData);
          // this.cartData = {astronaut};
        }); */
  }

  ngOnInit() {
    this.userId = JSON.parse(this.localStorage.getItem('currentUser')).uid;
    // call menu
    this.httpService.get('api/menu', {}).subscribe(res => {
      this.menuData = res.data;
    }, error => {
      console.log('Error');
    });
    // cart data
    this.getCartCount();
    // this.cartService.getCartCount();

  }

  getCartCount(){
    this.httpService.get('api/cart/get/' + this.userId, {}).subscribe(res => {
      console.log('Menu', res);
      this.cartData = res.data;
      this.cartService.updateCart(res.data);
    }, error => {
      console.log('Error');
    });
  }

  reloadTopPanel(event){
    console.log(event);
    this.getCartCount();
  }

  logout(){
    this.authService.logout();
  }

}
