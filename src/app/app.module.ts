import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER, ErrorHandler } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { LocationStrategy, HashLocationStrategy, PathLocationStrategy } from '@angular/common';
import { HttpModule, Http } from '@angular/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatMenuModule, MatIconModule, MatCardModule } from '@angular/material';

import { AppConfig } from './app.config';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { SimpleLayoutComponent } from './layout/simple-layout/simple-layout.component';
import { PageNotFoundComponent } from './pages/common/page-not-found.component';
import { LocalStorageService } from './_services/local-storage.service';
import { AuthGuard } from './_services/auth.guard';
import { LoginGuard } from './_services/login.guard';
import { AuthLayoutComponent } from './layout/auth-layout/auth-layout.component';
import { HttpService } from './_services/http.service';
import { AuthenticationService } from './_services/authentication.service';
import { AuthModule } from './layout/auth-layout/auth.module';
import { CartComponent } from './mod/cart/cart.component';
import { CartService } from './_services/cart-servics';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './_services/auth-interceptor';
import { MessageService } from './_services/message-servics';

export function initialConfigLoad(config: AppConfig) {
  return () => config.load();
}

const materialModules = [
  MatMenuModule, MatIconModule, MatCardModule
];

@NgModule({
  declarations: [
    AppComponent,
    SimpleLayoutComponent,
    PageNotFoundComponent,
    AuthLayoutComponent,
    CartComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule, ReactiveFormsModule,
    materialModules,
    HttpModule,
    AppRoutingModule,
    AuthModule,
    HttpClientModule
  ],
  exports: [
    CartComponent,
    materialModules ],
  providers: [LocalStorageService, HttpService, AuthGuard, LoginGuard, AuthenticationService, AppConfig, 
    CartService, MessageService,
  { provide: APP_INITIALIZER, useFactory: initialConfigLoad, deps: [AppConfig], multi: true }
],
  bootstrap: [AppComponent]
})
export class AppModule { }
