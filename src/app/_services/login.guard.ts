import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { LocalStorageService } from './local-storage.service';

@Injectable()
export class LoginGuard implements CanActivate {
    constructor(private router: Router,
                private localStorage: LocalStorageService ) {}

    canActivate() {
        if (this.localStorage.getItem('currentUser')) {
            this.router.navigate(['/']);
            return false;
        } else {
            return true;
        }
    }
}

