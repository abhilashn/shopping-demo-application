import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { LocalStorageService } from './local-storage.service';
/* import {HttpService} from './http.service';
import {LocalStorageService} from './local-storage.service';
import {AuthenticationService} from './authentication.service'; */

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router,
                private localStorage: LocalStorageService ) {}

    canActivate() {
        if (this.localStorage.getItem('currentUser')) {
            return true;
        } else {
            this.router.navigate(['/sign-in']);
            return false;
        }
    }
}

