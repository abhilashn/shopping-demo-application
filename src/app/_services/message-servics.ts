import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class MessageService {
    private msgUpdated = new Subject<any>();
    msgUpdated$ = this.msgUpdated.asObservable();

    constructor() {
    }

    updateMsg(data: any) {
        this.msgUpdated.next(data);
    }
}