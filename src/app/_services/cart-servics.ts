import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class CartService {
    private cartUpdated = new Subject<any>();
    cartUpdated$ = this.cartUpdated.asObservable();

    constructor() {
    }

    updateCart(data: any) {
        this.cartUpdated.next(data);
    }
}