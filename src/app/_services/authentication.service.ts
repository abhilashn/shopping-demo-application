import { Injectable, NgZone } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { HttpService } from './http.service';
import { LocalStorageService } from './local-storage.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { MessageService } from './message-servics';

@Injectable()
export class AuthenticationService {
    constructor(private http: Http,
        private zone: NgZone,
        private router: Router,
        private httpService: HttpService,
        private location: Location,
        private localStorage: LocalStorageService,
        private msgService: MessageService
    ) {
    }

    isLoggedIn(){
        if (this.localStorage.getItem('currentUser')) {
            return true;
        } else {
            return false;
        }
    }

    /* Used in -> login.component.ts */
    login(data,token) {
        this.localStorage.setItem('currentUser', JSON.stringify(data));
        this.localStorage.setItem('token', token);
        return true;
    }

    logout() {
        // remove user from local storage to log user out
        this.localStorage.removeItem('currentUser');
        this.localStorage.removeItem('token');

        /* this.httpService.get('logout').subscribe(
            data => {
                //  this.localStorage.removeItem('currentUser');
                this.msgService.updateMsg({'message': result.message, 'success': result.success});
            },
            error => {
                //  this.localStorage.removeItem('currentUser');
                this.msgService.updateMsg({'message': 'Something went wrong', 'success': false});
            }); */
        this.zone.runOutsideAngular(() => {
            // location.reload();
        });
        this.router.navigate(['/sign-in']);
        // window.location.reload();

        return true;
    }
}
