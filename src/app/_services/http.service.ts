import { Injectable, Inject, Output, EventEmitter, ViewChild, TemplateRef } from '@angular/core';
import { Http, Headers, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Router } from '@angular/router';
import { AppConfig } from '../app.config';
import { environment } from '../../environments/environment';

import { DOCUMENT } from '@angular/platform-browser';
import { LocalStorageService } from './local-storage.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class HttpService {
    // public CryptoJS = require("crypto-js");
    public apiUrl = this.appConfig.getConfig('apiUrl'); /* environment.apiUrl; */
    public baseUrl = this.doc.location.origin + this.doc.location.pathname + '#/';
    public pendingRequestsNumber: any = 0;

    constructor(private appConfig: AppConfig,
        private http: Http, private router: Router,
        @Inject(DOCUMENT) private doc: Document,
        private localStorage: LocalStorageService) {
        if (!this.localStorage.getItem('currentUser')) {

        }
        this.apiUrl = this.appConfig.getConfig('apiUrl'); /* environment.apiUrl; */
    }

    createAuthorizationHeader(headers: Headers) {
        headers.append('x-access-token', '' + this.localStorage.getItem('token')); 
      }
    

    post(url: string, data?: any) {

        this.pendingRequestsNumber++;
        let param: any = {};
            param = data;
        if (this.localStorage.getItem('token') && this.localStorage.getItem('currentUser')){
            param['token'] = this.localStorage.getItem('token');
            url += '?token='+this.localStorage.getItem('token')
        }
        return this.http.post(this.apiUrl + '' + url, param, this.jwt()).map(res => {
            this.pendingRequestsNumber--;
            return this.resStatus(res);
        }, err => {
            this.pendingRequestsNumber--;
        }).catch(e => {
            if (e.status === 401 || e.status === 403) {
                this.errorUnauthorized(e.json());
                console.log('Unauthorized');
                return Observable.throw('Unauthorized');
            }
            if (e.status === 0) {
                if (!this.localStorage.getItem('currentUser')) {
                    console.log('Not logged');
                    this.router.navigate(['sign-in']);
                }
            }
            this.pendingRequestsNumber--;
            this.resStatus(e);
            return Observable.throw('Unauthorized');
            // do any other checking for statuses here
        });
    }

    get(url: string, data?: any) {
        this.pendingRequestsNumber++;
        const params = new URLSearchParams();
        for (const key in data) {
            if (data[key]) {
                params.set(key, data[key]);
            }
        }
        let param: any = {};
        param = data;

        // Header for language
        const myHeaders = new Headers();
        if (this.localStorage.getItem('token') && this.localStorage.getItem('currentUser')){
            param['token'] = this.localStorage.getItem('token');
        }

        const options = new RequestOptions({
            headers: myHeaders,
            withCredentials: true,
            // Have to make a URLSearchParams with a query string
            search: param
        });

        return this.http.get(this.apiUrl + '' + url, options).map(res => {
            this.pendingRequestsNumber--;
            return this.resStatus(res);
        }, err => {
            this.pendingRequestsNumber--;
        }).catch(e => {
            this.pendingRequestsNumber--;
            if (e.status === 401 || e.status === 403) {
                this.errorUnauthorized(e.json());
                console.log('Unauthorized');
                return Observable.throw('Unauthorized');
            }
            if (e.status === 0) {
                if (!this.localStorage.getItem('currentUser')) {
                    console.log('Not logged');
                    this.router.navigate(['sign-in']);
                } else {
                    return Observable.throw('Unauthorized');
                }
            }
            this.resStatus(e);
            return Observable.throw('connection');
            // do any other checking for statuses here
        });
    }

    errorUnauthorized(err: any) {
        this.removeToken();
        console.log('err', err);
    }

    resStatus(res: any) {
        let resData = null;
        resData = res.json();
        console.log('resData', resData);
        if (resData.success && resData.message) {
            console.log('resData', resData);
        }
        if (res.status === 0) {
            if (!this.localStorage.getItem('currentUser')) {
                console.log('Session Terminated');
                this.router.navigate(['sign-in']);
            } else {
                console.log('Service Expired');
            }
        }
        if (res.status < 200 || res.status >= 300) {
            if (res.status === 401 || res.status === 403) {
                console.log('Not logged');
                this.removeToken();
                this.router.navigate(['sign-in']);
            }
            console.log('Error', res);
            return false;

        }
        if (res.status === 200) {
            if (resData.success) {
                return resData;
            } else {
                console.log('Result false', res);
                // this.toasterService.pop('error', 'Error', resData.message);
                if (resData.data && resData.data.errors) {
                    for (const key in resData.data.errors) {
                        if (resData.data.errors[key].ErrorMessage) {
                            console.log('Error list', key + ' : ' + resData.data.errors[key].ErrorMessage);
                        }
                    }
                }
                if (resData.data && resData.data.UniqueValidationFails) {
                    console.log('Error 1');
                }
                return resData;
            }
        }

    }

    private jwt() {
        let myHeaders = new Headers();
        if (this.localStorage.getItem('token') && this.localStorage.getItem('currentUser')){
            myHeaders.append('x-access-token', this.localStorage.getItem('token'));
            myHeaders.set('x-access-token', this.localStorage.getItem('token'));
        }
        console.log(myHeaders);
        return new RequestOptions({ headers: myHeaders, withCredentials: true });
    }

    public getJSON(filePath) {
        return this.http.get(filePath).map(res => {
            return res.json();
        }, err => {
        });
    }

    public removeToken(){
        this.localStorage.removeItem('currentUser');
        this.localStorage.removeItem('token');
    }


}
